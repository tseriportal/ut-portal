.. UT-Portal documentation master file, created by
   sphinx-quickstart on Mon Jun  5 08:54:14 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to UT-Portal's documentation!
=====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
.. include:: portal.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

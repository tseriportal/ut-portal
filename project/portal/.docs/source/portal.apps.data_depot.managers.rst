portal\.apps\.data\_depot\.managers package
===========================================

Submodules
----------

portal\.apps\.data\_depot\.managers\.base module
------------------------------------------------

.. automodule:: portal.apps.data_depot.managers.base
    :members:
    :undoc-members:
    :show-inheritance:

portal\.apps\.data\_depot\.managers\.private\_data module
---------------------------------------------------------

.. automodule:: portal.apps.data_depot.managers.private_data
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: portal.apps.data_depot.managers
    :members:
    :undoc-members:
    :show-inheritance:

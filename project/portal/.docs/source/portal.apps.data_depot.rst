portal\.apps\.data\_depot package
=================================

Subpackages
-----------

.. toctree::

    portal.apps.data_depot.api
    portal.apps.data_depot.managers

Submodules
----------

portal\.apps\.data\_depot\.admin module
---------------------------------------

.. automodule:: portal.apps.data_depot.admin
    :members:
    :undoc-members:
    :show-inheritance:

portal\.apps\.data\_depot\.apps module
--------------------------------------

.. automodule:: portal.apps.data_depot.apps
    :members:
    :undoc-members:
    :show-inheritance:

portal\.apps\.data\_depot\.models module
----------------------------------------

.. automodule:: portal.apps.data_depot.models
    :members:
    :undoc-members:
    :show-inheritance:

portal\.apps\.data\_depot\.tests module
---------------------------------------

.. automodule:: portal.apps.data_depot.tests
    :members:
    :undoc-members:
    :show-inheritance:

portal\.apps\.data\_depot\.urls module
--------------------------------------

.. automodule:: portal.apps.data_depot.urls
    :members:
    :undoc-members:
    :show-inheritance:

portal\.apps\.data\_depot\.views module
---------------------------------------

.. automodule:: portal.apps.data_depot.views
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: portal.apps.data_depot
    :members:
    :undoc-members:
    :show-inheritance:

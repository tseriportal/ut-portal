"""
We import every settings we need here
"""
from portal.settings.settings_common import *
from portal.settings.settings_secret import *
from portal.settings.settings_local import *
from portal.settings.settings_logging import *
from portal.settings.settings_agave import *
from portal.settings.settings_django_cms import *
from portal.settings.settings_celery import *
from portal.settings.data_depot import *
from portal.settings.elasticsearch import *

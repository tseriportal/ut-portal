"""Agave setup.
"""
AGAVE_TENANT_ID = 'tacc.prod'
AGAVE_TENANT_BASEURL = 'https://api.tacc.utexas.edu'
#
# Agave Client Configuration
AGAVE_CLIENT_KEY = 'TH1$_!$-MY=K3Y!~'
AGAVE_CLIENT_SECRET = 'TH1$_!$-My=S3cr3t!~'

AGAVE_SUPER_TOKEN = 'S0m3T0k3n_tHaT-N3v3r=3xp1R35'

AGAVE_STORAGE_SYSTEM = 'utportal.storage.default'
AGAVE_COMMUNITY_DATA_SYSTEM = 'utportal.storage.default'

#This should be the "auth" field when creating storage systems
PROJECT_SYSTEM_STORAGE_CREDENTIALS = {}

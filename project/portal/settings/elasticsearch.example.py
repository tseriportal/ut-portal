"""Elasticsearch configuration"""
ES_HOSTS = ["elasticsearch"]
ES_DEFAULT_INDEX = "files"
ES_DEFAULT_INDEX_ALIAS = "default"
ES_PUBLIC_INDEX = "publications"
ES_PUBLIC_INDEX_ALIAS = "public"
ES_FILES_DOC_TYPE = "files"
ES_PROJECTS_DOC_TYPE = "projects"
ES_METADATA_DOC_TYPE = "metadata"

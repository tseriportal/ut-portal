"""
Place local settings here
"""
import os
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

DEBUG = True

SITE_ID = 1


# database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'portal_db',
        'USER': 'portal',
        'PASSWORD': 'p0rt4l',
        'HOST': 'mysql',
        #'PORT': '5432',
    }
}

# ROOT paths
STATIC_ROOT = os.path.join(BASE_DIR, '../static')
MEDIA_ROOT = os.path.join(BASE_DIR, '../media')

# EMAIL SETUP
# for more info: https://docs.djangoproject.com/en/1.11/topics/email/#configuring-email-for-development
EMAIL_HOST = ''
EMAIL_PORT = ''
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
EMAIL_USE_TLS = False
EMAIL_USER_SSL = False
DEFAULT_FROM_EMAIL = ''

"""
Settings specific for django cms
"""

CMS_TEMPLATES = (
    ('test_template.html', 'Template One'),
)

LANGUAGES = [
    ('en-us', 'US English')
]

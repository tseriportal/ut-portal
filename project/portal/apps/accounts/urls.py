"""
.. module:: portal.apps.accounts.urls
   :synopsis: Accounts URLs
"""
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.contrib import admin
from portal.apps.accounts.views import (IndexView, LoginView,
                                        LogoutView, RegisterView)
from portal.apps.accounts import views

urlpatterns = [
    url(r'^login/?', LoginView.as_view(), name='login'),
    url(r'^logout/?', LogoutView.as_view(), name='logout'),
    # url(r'^register/?', RegisterView.as_view(), name='register'),
    url(r'^register/?', views.register, name='register'),
    url(r'^', IndexView.as_view(), name='index'),
]

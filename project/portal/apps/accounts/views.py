"""
Accounts views.
"""
from django.contrib.auth import logout
from django.contrib import messages
from django.views.generic.base import TemplateView, View
from django.shortcuts import redirect, render
from django.conf import settings
from django.http import HttpResponseRedirect

from portal.apps.accounts import forms, integrations

import logging

logger = logging.getLogger(__name__)

# Create your views here.

class IndexView(TemplateView):
    """Main accounts view.
    """
    template_name = 'portal/apps/accounts/index.html'

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            return redirect('/accounts/login')

        return super(IndexView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        return context


class LoginView(TemplateView):
    """Login options view
    """
    template_name = 'portal/apps/accounts/login.html'

    def get_context_data(self, **kwargs):
        context = super(LoginView, self).get_context_data(**kwargs)
        return context


class LogoutView(View):
    """Logout view
    """
    def get(self, request):
        logout(request)
        return HttpResponseRedirect(settings.LOGIN_URL)


class RegisterView(TemplateView):
    """Register View
    """
    template_name = 'portal/apps/accounts/register.html'


def register(request):
    if request.user.is_authenticated():
        messages.info(request, 'You are already logged in!')
        return HttpResponseRedirect('designsafe_accounts:index')

    if request.method == 'POST':
        account_form = forms.UserRegistrationForm(request.POST)
        if account_form.is_valid():
            try:
                account_form.save()
                return HttpResponseRedirect(
                    reverse('designsafe_accounts:registration_successful'))
            except Exception as e:
                logger.info('error: {}'.format(e))

                error_type = e.args[1] if len(e.args) > 1 else ''

                if 'DuplicateLoginException' in error_type:
                    err_msg = (
                        'The username you chose has already been taken. Please '
                        'choose another. If you already have an account with TACC, '
                        'please log in using those credentials.')
                    account_form._errors.setdefault('username', [err_msg])
                elif 'DuplicateEmailException' in error_type:
                    err_msg = (
                        'This email is already registered. If you already have an '
                        'account with TACC, please log in using those credentials.')
                    account_form._errors.setdefault('email', [err_msg])
                    err_msg = '%s <a href="%s">Did you forget your password?</a>' % (
                        err_msg,
                        reverse('designsafe_accounts:password_reset'))
                elif 'PasswordInvalidException' in error_type:
                    err_msg = (
                        'The password you provided did not meet the complexity '
                        'requirements.')
                    account_form._errors.setdefault('password', [err_msg])
                else:

                    safe_data = account_form.cleaned_data.copy()
                    safe_data['password'] = safe_data['confirmPassword'] = '********'
                    logger.exception('User Registration Error!', extra=safe_data)
                    err_msg = (
                        'An unexpected error occurred. If this problem persists '
                        'please create a support ticket.')
                messages.error(request, err_msg)
        else:
            messages.error(request, 'There were errors processing your registration. '
                                    'Please see below for details.')
    else:
        account_form = forms.UserRegistrationForm()

    context = {
        'account_form': account_form
    }
    return render(request, 'portal/apps/accounts/register.html', context)


def registration_successful(request):
    return render_to_response('designsafe/apps/accounts/registration_successful.html')

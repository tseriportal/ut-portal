"""
.. module: portal.apps.data_depot.managers.private_data
   :synopsis: Manager handling Shared data.
"""
from __future__ import unicode_literals, absolute_import
import os
import logging
from future.utils import python_2_unicode_compatible
from django.conf import settings
from django.contrib.auth import get_user_model
from portal.apps.data_depot.managers.base import AgaveFileManager
# from portal.libs.elasticsearch.docs.files import BaseFile
from portal.libs.agave.models.files import BaseFile
from portal.libs.elasticsearch.serializers import BaseAgaveSerializer
from portal.utils.exceptions import ApiMethodNotAllowed

#pylint: disable=invalid-name
logger = logging.getLogger(__name__)
#pylint: enable=invalid-name

@python_2_unicode_compatible
class FileManager(AgaveFileManager):
    """File Manager handling shared data.
    """
    def __init__(self, request=None, **kwargs):#pylint: disable=super-init-not-called
        """Initializing client data

        Saving necessary details from request for further searches.

        :param request: Django request object.

        """
        try:
            client = kwargs.get('client',
                                request.user.agave_oauth.client)
            self.username = kwargs.get('username',
                                       request.user.username)
            self.session_id = kwargs.get('session_id',
                                         request.session.session_key)
            self.serializer_cls = BaseAgaveSerializer
        except AttributeError:
            raise
            #community_user = settings.AGAVE_COMMUNITY_ACCOUNT
            #self._ac = get_user_model().\
            #           objects.get(username=community_user)
        super(FileManager, self).__init__(client, **kwargs)

    def _parse_file_id(self, file_id):
        """Parse a file id.

        :param str file_id: Id representing a file.

        :return: system, path.
        :rtype: tuple

        .. note:: The file id sent by the front-end should be of the form:
        <system>/<file_path>

        """
        id_comps = file_id.strip('/').split('/')
        system = id_comps[0]
        if len(id_comps[1:]):
            file_path = os.path.join(*id_comps[1:])
        else:
            file_path = '/'

        return (system, file_path)

    def get_file(self, file_id, **kwargs):
        """Convinience method to initialize a file quickly.

        :param str file_id: Id representing a file.

        :return: A file obj.
        :rtype: :class:`~libs.agave.models.files.BaseFile`
        """
        system, path = self._parse_file_id(file_id)
        _file = BaseFile(self._ac, system, path)
        return _file

    @property
    def encoder_cls(self):
        """Returns encoder cls"""
        return self.serializer_cls

    @property
    def requires_auth(self):
        """Weather it should check for an authenticated user.

        If this is a public data file manager, it should return False.
        """
        return True

    def listing(self, file_id, **kwargs):
        """List file or folder contents.

        :param str file_id: Id representing file/folder listed.

        :returns: A listing object. See :class:`~libs.agave.models.file.BaseFile`.
        :rtype: obj
        """
        _file = self.get_file(file_id)
        page = kwargs.get('page', None)
        if page is not None:
            offset = page * settings.PORTAL_DATA_DEPOT_PAGE_SIZE
        else:
            offset = kwargs.get('offset', 0)

        limit = kwargs.get('limit', settings.PORTAL_DATA_DEPOT_PAGE_SIZE)
        _file.children(offset, limit)
        return _file

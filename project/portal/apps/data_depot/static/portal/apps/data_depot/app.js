(function(window, angular) {
  var module = angular.module('designsafe');
  module.requires.push(
    'ui.router',
    'ui.bootstrap',
    'django.context'
  );

  function config($httpProvider, $locationProvider, $stateProvider, $urlRouterProvider, $urlMatcherFactoryProvider, Django, SystemsService) {

    $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
    $locationProvider.html5Mode(true);
    $urlMatcherFactoryProvider.strictMode(false);

    //angular.extend(toastrConfig, {
    //  positionClass: 'toast-bottom-left',
    //  timeOut: 20000
    //});

    $stateProvider
      /* Private */
      .state(
        'db', {
          url: '',
          abstract: true,
          resolve: {
            'systems': ['SystemsService', function(SystemsService) {
              return SystemsService.listing();
            }]
          }
        }
      )
      .state('db.myData', {
        url: '/agave/{systemId}/{filePath:any}/',
        controller: 'MyDataCtrl',
        templateUrl: '/static/portal/apps/data_depot/templates/agave-data-listing.html',
        resolve: {
          'listing': ['$stateParams', 'DataBrowserService', 'SystemsService', function($stateParams, DataBrowserService, SystemsService) {
            var options = {
              system: ($stateParams.systemId),
              path: ($stateParams.filePath)
            };
            // TODO: Fix this listing thing
            DataBrowserService.apiParams.fileMgr = 'my-data';
            DataBrowserService.apiParams.baseUrl = '/api/data-depot/files';
            DataBrowserService.apiParams.searchState = 'dataSearch';
            return DataBrowserService.browse(options);
          }],
          'auth': function($q) {
            if (Django.context.authenticated) {
              return true;
            } else {
              return $q.reject({
                type: 'authn',
                context: Django.context
              });
            }
          }
        }
      })
      .state('dataSearch',{
        url: '/agave-search/?query_string&offset&limit',
        controller: 'MyDataCtrl',
        templateUrl: '/static/scripts/data-depot/templates/agave-search-data-listing.html',
        params: {
          systemId: 'utportal.storage.default',
          filePath: '$SEARCH'
        },
        resolve: {
          'listing': ['$stateParams', 'DataBrowserService', function($stateParams, DataBrowserService) {
            var systemId = $stateParams.systemId || 'utportal.storage.default';
            var filePath = $stateParams.filePath || Django.user;
            DataBrowserService.apiParams.fileMgr = 'agave';
            DataBrowserService.apiParams.baseUrl = '/api/agave/files';
            DataBrowserService.apiParams.searchState = 'dataSearch';
            var queryString = $stateParams.query_string;
            if (/[^A-Za-z0-9]/.test(queryString)){
              queryString = '"' + queryString + '"';
            }
            var options = {system: $stateParams.systemId, query_string: queryString, offset: $stateParams.offset, limit: $stateParams.limit};
            return DataBrowserService.search(options);
          }],
          'auth': function($q) {
            if (Django.context.authenticated) {
              return true;
            } else {
              return $q.reject({
                type: 'authn',
                context: Django.context
              });
            }
          }
        }
      })
      .state('sharedData', {
        url: '/shared/{systemId}/{filePath:any}/',
        controller: 'SharedDataCtrl',
        templateUrl: '/static/scripts/data-depot/templates/agave-shared-data-listing.html',
        params: {
          systemId: 'utportal.storage.default',
          filePath: '$SHARE'
        },
        resolve: {
          'listing': ['$stateParams', 'DataBrowserService', function($stateParams, DataBrowserService) {
            var systemId = $stateParams.systemId || 'utportal.storage.default';
            var filePath = $stateParams.filePath || '$SHARE/';

            DataBrowserService.apiParams.fileMgr = 'agave';
            DataBrowserService.apiParams.baseUrl = '/api/agave/files';
            DataBrowserService.apiParams.searchState = 'sharedDataSearch';
            return DataBrowserService.browse({system: systemId, path: filePath});
          }],
          'auth': function($q) {
            if (Django.context.authenticated) {
              return true;
            } else {
              return $q.reject({
                type: 'authn',
                context: Django.context
              });
            }
          }
        }
      })
      .state('sharedDataSearch',{
        url: '/shared-search/?query_string&offset&limit&shared',
        controller: 'MyDataCtrl',
        templateUrl: '/static/scripts/data-depot/templates/agave-search-data-listing.html',
        params: {
          systemId: 'utportal.storage.default',
          filePath: '$SEARCHSHARED',
          shared: 'true'
        },
        resolve: {
          'listing': ['$stateParams', 'DataBrowserService', function($stateParams, DataBrowserService) {
            var systemId = $stateParams.systemId || 'utportal.storage.default';
            var filePath = $stateParams.filePath || Django.user;
            DataBrowserService.apiParams.fileMgr = 'agave';
            DataBrowserService.apiParams.baseUrl = '/api/agave/files';
            DataBrowserService.apiParams.searchState = 'sharedDataSearch';
            var queryString = $stateParams.query_string;
            if (/[^A-Za-z0-9]/.test(queryString)){
              queryString = '"' + queryString + '"';
            }
            var options = {system: $stateParams.systemId, query_string: queryString, offset: $stateParams.offset, limit: $stateParams.limit, shared: $stateParams.shared};
            return DataBrowserService.search(options);
          }],
          'auth': function($q) {
            if (Django.context.authenticated) {
              return true;
            } else {
              return $q.reject({
                type: 'authn',
                context: Django.context
              });
            }
          }
        }
      })
      .state('db.projects', {
        url: '/projects/',
        // controller: 'ProjectRootCtrl',
        // templateUrl: '/static/portal/apps/data_depot/templates/project-root.html'
        abstract: true,

      })
      .state('db.projects.list', {
        url: '',
        controller: 'ProjectListCtrl',
        templateUrl: '/static/portal/apps/data_depot/templates/project-list.html'
      })
      .state('db.projects.listing', {
        url: '{systemId}/{filePath:any}',
        controller: 'ProjectListingCtrl',
        templateUrl: '/static/portal/apps/data_depot/templates/agave-data-listing.html',
        resolve: {
          'listing': ['$stateParams', 'DataBrowserService', 'SystemsService', function($stateParams, DataBrowserService, SystemsService) {
            var options = {
              system: ($stateParams.systemId),
              path: ($stateParams.filePath)
            };
            // TODO: Fix this listing thing
            DataBrowserService.apiParams.fileMgr = 'shared';
            DataBrowserService.apiParams.baseUrl = '/api/data-depot/files';
            DataBrowserService.apiParams.searchState = 'dataSearch';
            return DataBrowserService.browse(options);
          }],
        }
      })
      .state('db.projects.view', {
        url: '{projectId}/',
        abstract: true,
        controller: 'ProjectViewCtrl',
        templateUrl: '/static/portal/apps/data_depot/templates/project-view.html',
        resolve: {
          'projectId': function($stateParams) { return $stateParams.projectId; }
        }
      })
      .state('db.projects.view.data', {
        url: '{filePath:any}',
        controller: 'ProjectDataCtrl',
        templateUrl: '/static/portal/apps/data_depot/templates/project-data.html',
        params: {
          projectTitle: ''
        },
        resolve: {
          'projectId': function($stateParams) { return $stateParams.projectId; },
          'filePath': function($stateParams) { return $stateParams.filePath || '/'; },
          'projectTitle': function($stateParams) { return $stateParams.projectTitle; }
        }
      })
      .state('myPublications', {
        url: '/my-publications/{publicationId}}/{fileId:any}/',
        templateUrl: '/static/scripts/data-depot/templates/enhanced-data-listing.html'
      })
      .state('boxData', {
        url: '/box/{filePath:any}',
        controller: 'ExternalDataCtrl',
        templateUrl: '/static/scripts/data-depot/templates/box-data-listing.html',
        params: {
          filePath: '',
          name: 'Box',
          customRootFilePath: 'box/'
        },
        resolve: {
          'listing': ['$stateParams', 'DataBrowserService', function($stateParams, DataBrowserService) {
            var filePath = $stateParams.filePath || '/';
            DataBrowserService.apiParams.fileMgr = 'box';
            DataBrowserService.apiParams.baseUrl = '/api/external-resources/files';
            DataBrowserService.apiParams.searchState = undefined;
            return DataBrowserService.browse({path: filePath});
          }],
          'auth': function($q) {
            if (Django.context.authenticated) {
              return true;
            } else {
              return $q.reject({
                type: 'authn',
                context: Django.context
              });
            }
          }
        }
      })
      .state('dropboxData', {
        url: '/dropbox/{filePath:any}',
        controller: 'ExternalDataCtrl',
        templateUrl: '/static/scripts/data-depot/templates/dropbox-data-listing.html',
        params: {
          filePath: '',
          name: 'Dropbox',
          customRootFilePath: 'dropbox/'
        },
        resolve: {
          'listing': ['$stateParams', 'DataBrowserService', function($stateParams, DataBrowserService) {
            var filePath = $stateParams.filePath || '/';
            DataBrowserService.apiParams.fileMgr = 'dropbox';
            DataBrowserService.apiParams.baseUrl = '/api/external-resources/files';
            DataBrowserService.apiParams.searchState = undefined;
            return DataBrowserService.browse({path: filePath});
          }],
          'auth': function($q) {
            if (Django.context.authenticated) {
              return true;
            } else {
              return $q.reject({
                type: 'authn',
                context: Django.context
              });
            }
          }
        }
      })

      /* Public */
      .state('publicDataSearch',{
        url: '/public-search/?query_string&offset&limit',
        controller: 'PublicationDataCtrl',
        templateUrl: '/static/scripts/data-depot/templates/search-public-data-listing.html',
        params: {
          systemId: 'nees.public',
          filePath: '$SEARCH'
        },
        resolve: {
          'listing': ['$stateParams', 'DataBrowserService', function($stateParams, DataBrowserService) {
            var systemId = $stateParams.systemId || 'nees.public';
            var filePath = $stateParams.filePath || '/';
            DataBrowserService.apiParams.fileMgr = 'public';
            DataBrowserService.apiParams.baseUrl = '/api/public/files';
            DataBrowserService.apiParams.searchState = 'publicDataSearch';
            var queryString = $stateParams.query_string;
            if (/[^A-Za-z0-9]/.test(queryString)){
              queryString = '"' + queryString + '"';
            }
            var options = {system: $stateParams.systemId, query_string: queryString, offset: $stateParams.offset, limit: $stateParams.limit};
            return DataBrowserService.search(options);
          }],
          'auth': function($q) {
              return true;
          }
        }
      })
      .state('db.communityData', {
        url: '/public/{systemId}/{filePath:any}',
        controller: 'CommunityDataCtrl',
        templateUrl: '/static/portal/apps/data_depot/templates/agave-data-listing.html',
        params: {
          systemId: 'utportal.storage.community',
          filePath: '/'
        },
        resolve: {
          'listing': ['$stateParams', 'DataBrowserService', 'SystemsService', function($stateParams, DataBrowserService, SystemsService) {
            var options = {
              system: ($stateParams.systemId),
              path: ($stateParams.filePath || '/')
            };

            DataBrowserService.apiParams.fileMgr = 'shared';
            DataBrowserService.apiParams.baseUrl = '/api/data-depot/files';
            DataBrowserService.apiParams.searchState = 'dataSearch';
            return DataBrowserService.browse(options);
            // if (options.path === '/') {
              // options.path = Django.user;
            // }

          }],
        }
      })
      .state('publicData', {
        url: '/public/nees.public/{filePath:any}',
        controller: 'PublicationDataCtrl',
        templateUrl: '/static/scripts/data-depot/templates/agave-public-data-listing.html',
        params: {
          systemId: 'nees.public',
          filePath: ''
        },
        resolve: {
          'listing': ['$stateParams', 'DataBrowserService', function($stateParams, DataBrowserService) {
            var systemId = $stateParams.systemId || 'nees.public';
            var filePath = $stateParams.filePath || '/';
            DataBrowserService.apiParams.fileMgr = 'public';
            DataBrowserService.apiParams.baseUrl = '/api/public/files';
            DataBrowserService.apiParams.searchState = 'publicDataSearch';
            return DataBrowserService.browse({system: systemId, path: filePath});
          }],
          'auth': function($q) {
              return true;
          },
          userAuth: [function () {
              return true;
          }]
        }
      })
      .state('publishedData', {
        url: '/public/utportal.storage.published/{filePath:any}',
        controller: 'PublishedDataCtrl',
        templateUrl: '/static/scripts/data-depot/templates/published-data-listing.html',
        params: {
          systemId: 'utportal.storage.published',
          filePath: '',
        },
        resolve: {
          'listing': ['$stateParams', 'DataBrowserService', function($stateParams, DataBrowserService){
            var systemId = $stateParams.systemId || 'utportal.storage.published';
            var filePath = $stateParams.filePath;
            DataBrowserService.apiParams.fileMgr = 'published';
            DataBrowserService.apiParams.baseUrl = '/api/public/files';
            DataBrowserService.apiParams.searchState = undefined;
            return DataBrowserService.browse({system: systemId, path: filePath});
          }],
          'auth': function($q){
              return true;
          },
          userAuth: [function () {
              return true;
          }]
        }
      })
      .state('trainingMaterials', {
        url: '/training/',
        template: '<pre>local/trainingMaterials.html</pre>'
      })
    ;


    // TODO: Fix this system listing thing
    $urlRouterProvider.otherwise(function($injector, $location, SystemsService) {
      var $state = $injector.get('$state');
      var SystemsService = $injector.get('SystemsService');
      /* Default to MyData for authenticated users, PublicData for anonymous */
      if (Django.context.authenticated) {
        SystemsService.listing().then(function (resp) {
          console.log(resp);
          my_data = _.find(resp, {name: 'My Data'});
          $state.go('db.myData', {
            systemId: my_data.systemId,
            filePath: ''
          });
        });

      } else {
        $state.go('publicData');
      }
    });
  }

  module
    .config(['$httpProvider', '$locationProvider', '$stateProvider', '$urlRouterProvider', '$urlMatcherFactoryProvider', 'Django', config])
    .run(['$rootScope', '$location', '$state', 'Django', function($rootScope, $location, $state, Django) {
      $rootScope.$state = $state;

      $rootScope.$on('$stateChangeStart', function(event, toState, toParams) {
        if (toState.name === 'myData' || toState.name === 'sharedData') {
          var ownerPath = new RegExp('^/?' + Django.user).test(toParams.filePath);
          if (toState.name === 'myData' && !ownerPath) {
            event.preventDefault();
            $state.go('sharedData', toParams);
          } else if (toState.name === 'sharedData' && ownerPath) {
            event.preventDefault();
            $state.go('myData', toParams);
          }
        }
      });

      // $rootScope.$on('$stateChangeSuccess', function() {});

      $rootScope.$on("$stateChangeError", function(event, toState, toParams, fromState, fromParams, error) {
        if (error.type === 'authn') {
          var redirectUrl = $state.href(toState.name, toParams);
          window.location = '/login/?next=' + redirectUrl;
        }
      });
    }]);

//  module
//    .config(['WSBusServiceProvider', function(WSBusServiceProvider){
//
//        WSBusServiceProvider.setUrl(
//            (window.location.protocol === 'https:' ? 'wss://' : 'ws://') +
//            window.location.hostname +
//            (window.location.port ? ':' + window.location.port : '') +
//            '/ws/websockets?subscribe-broadcast&subscribe-user'
//        );
//	}]);
// 	.run(['WSBusService', 'Logging', function init(WSBusService, logger){
// 	  WSBusService.init(WSBusService.url);
//     }]);

//   module
// 	.run(['NotificationService', 'Logging', function init(NotificationService, logger){
// 	  NotificationService.init();
// }]);

})(window, angular);

(function(window, angular, $, _) {
  "use strict";

  var module = angular.module('designsafe');

  module.service('SystemsService', ['$http', '$q', function($http, $q) {

    var self = this;
    self.systems = null;

    this.listing = function () {
      return $http.get('/api/data-depot/systems/list').then(function (resp) {
        self.systems = resp.data.response;
        return resp.data.response;
      });
    };
  }]);

})(window, angular, jQuery, _);

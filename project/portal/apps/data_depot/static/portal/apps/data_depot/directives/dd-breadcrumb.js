/**
 *
 */
(function(window, angular) {

  var module = angular.module('designsafe');

  module.directive('ddBreadcrumb', function() {
    return {
      restrict: 'E',
      transclude: true,
      replace: true,
      templateUrl: '/static/portal/apps/data_depot/templates/dd-breadcrumb.html',
      scope: {
        listing: '=',
        skipRoot: '=',
        customRoot: '=',
        project: '=',
        onBrowse: '&',
        itemHref: '&'
      },
      link: function(scope) {
        scope.offset = 0;
        if (scope.skipRoot || scope.customRoot) {
          scope.offset = 1;
        }
      }
    };
  });

})(window, angular);

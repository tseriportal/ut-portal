import logging
import json
from portal.celery_app import app

logger = logging.getLogger(__name__)
@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))

"""Workpace API Urls
"""
from django.conf.urls import url
from portal.apps.workspace.api import views

urlpatterns = [
    url(r'^(?P<service>[a-z]+?)/$', views.call_api, name='call_api'),
]

"""
.. :module:: apps.workspace.api.views
   :synopsys: Views to handle Workspace API
"""
from __future__ import unicode_literals, absolute_import
import logging
import json
import urllib
import six
from datetime import datetime
from django.http import JsonResponse
from django.conf import settings
from portal.apps.workspace.api import lookups as LookupManager
from portal.views.base import BaseApiView
from portal.exceptions.api import ApiException

from agavepy.agave import AgaveException, Agave
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from requests import HTTPError
from urlparse import urlparse
from django.http import HttpResponse
from django.core.urlresolvers import reverse
from django.core.serializers.json import DjangoJSONEncoder

import time

#pylint: disable=invalid-name
logger = logging.getLogger(__name__)
METRICS = logging.getLogger('metrics.{}'.format(__name__))
#pylint: enable=invalid-name

class Error(Exception):  # I added this
    """Base class for exceptions in this module."""
    pass

class JobSubmitError(Error):  # I added this
    """Exception raised for errors in the input.

    Attributes:
        expression -- input expression in which the error occurred
        message -- explanation of the error
    """
    def json(self):
        return "jsonResponse"

    def __init__(self, expression, message):
        self.expression = expression
        self.message = message

def get_manager(request, file_mgr_name):
    """Lookup Manager to handle call"""
    fmgr_cls = LookupManager.lookup_manager(file_mgr_name)
    fmgr = fmgr_cls(request)
    if fmgr.requires_auth and not request.user.is_authenticated:
        raise ApiException("Login Required", status=403)
    return fmgr

LICENSE_TYPES = (
    ('MATLAB', 'MATLAB')
)

def _app_license_type(app_id):
    app_lic_type = app_id.split('-')[0].upper()
    lic_type = next((t[0] for t in LICENSE_TYPES if t[0] == app_lic_type), None)
    return lic_type

def call_api(request, service, *args, **kwargs):
    """Main Workspace view"""
    appIdGlobal = ""
    appVersionGlobal = ""
    try:
        agave = request.user.agave_oauth.client
        if service == 'apps':
            app_id = request.GET.get('app_id')
	    app_version = request.GET.get('app_version')
            appIdGlobal = app_id
            appVersionGlobal = app_version
            logger.debug("%s", str(app_id))
            logger.debug("%s", str(app_version))
            if app_id:
                #data = agave.apps.get(appId=app_id+"-"+app_version)
                data = agave.meta.listMetadata(q=json.dumps({"name": "ds_apps", "value.definition.id": app_id}))
                #time.sleep(5)
                data = data[0].value
                data = data['definition']
                logger.debug("%s", str(data))
                lic_type = _app_license_type(app_id)
                data['license'] = {
                    'type': lic_type
                }
                if lic_type is not None:
                    lic = request.user.licenses.filter(license_type=lic_type).first()
                    data['license']['enabled'] = lic is not None

            else:

                public_only = request.GET.get('publicOnly')
                if public_only == 'true':
                    data = agave.apps.list(publicOnly='true')
                else:
                    data = agave.apps.list()

        elif service == 'monitors':
            target = request.GET.get('target')
            ds_admin_client = Agave(api_server=getattr(settings, 'AGAVE_TENANT_BASEURL'),
				    token=getattr(settings, 'AGAVE_SUPER_TOKEN'))
            data = ds_admin_client.monitors.list(target=target)

        elif service == 'meta':
            app_id = request.GET.get('app_id')
	    logger.debug("%s", str(app_id))
            if request.method == 'GET':
                if app_id:
                    data = agave.meta.get(appId=app_id)
                    lic_type = _app_license_type(app_id)
                    data['license'] = {
                        'type': lic_type
                    }
                    if lic_type is not None:
                        lic = request.user.licenses.filter(license_type=lic_type).first()
                        data['license']['enabled'] = lic is not None

                else:
                    query = request.GET.get('q')
	            #logger.debug("%s", "Else")
		    #logger.debug("%s", str(query))
                    data = agave.meta.listMetadata(q=query)
                    #logger.debug("%s", str(data))
                    #data = dataList[0]
                    #logger.debug("%s", str(data))
            elif request.method == 'POST':
                meta_post = json.loads(request.body)
                meta_uuid = meta_post.get('uuid')

                if meta_uuid:
                    del meta_post['uuid']
                    data = agave.meta.updateMetadata(uuid=meta_uuid, body=meta_post)
                else:
                    data = agave.meta.addMetadata(body=meta_post)
            elif request.method == 'DELETE':
                meta_uuid = request.GET.get('uuid')
                if meta_uuid:
                    data = agave.meta.deleteMetadata(uuid=meta_uuid)


        # TODO: Need auth on this DELETE business
        elif service == 'jobs':
            if request.method == 'DELETE':
                job_id = request.GET.get('job_id')
                data = agave.jobs.delete(jobId=job_id)
            elif request.method == 'POST':
                job_post = json.loads(request.body)
                job_id = job_post.get('job_id')
                app_id = request.POST.get('appId')
                logger.debug("%s",appIdGlobal)
                logger.debug("%s",appVersionGlobal)
                #job_post['appId'] = app_id+"-"+app_version
                #job_post['appId'] = job_post['appId']+"-1.0"
                logger.debug("%s",job_post)
                logger.debug("%s",job_post['appId'])

                # cancel job / stop job
                if job_id:
                    data = agave.jobs.manage(jobId=job_id, body='{"action":"stop"}')

                # submit job
                elif job_post:

                    # cleaning archive path value
                    if 'archivePath' in job_post:
                        parsed = urlparse(job_post['archivePath'])
                        if parsed.path.startswith('/'):
                            # strip leading '/'
                            archive_path = parsed.path[1:]
                        else:
                            archive_path = parsed.path

                        if not archive_path.startswith(request.user.username):
                            archive_path = '{}/{}'.format(
                                request.user.username, archive_path)

                        job_post['archivePath'] = archive_path

                        if parsed.netloc:
                            job_post['archiveSystem'] = parsed.netloc
                    else:
                        job_post['archivePath'] = \
                            '{}/archive/jobs/{}/${{JOB_NAME}}-${{JOB_ID}}'.format(
                                request.user.username,
                                datetime.now().strftime('%Y-%m-%d'))

                    # check for running licensed apps
                    lic_type = _app_license_type(job_post['appId'])
                    if lic_type is not None:
                        lic = request.user.licenses.filter(license_type=lic_type).first()
                        job_post['parameters']['_license'] = lic.license_as_str()

                    # url encode inputs
                    if job_post['inputs']:
                        for key, value in six.iteritems(job_post['inputs']):
                            parsed = urlparse(value)
                            if parsed.scheme:
                                job_post['inputs'][key] = '{}://{}{}'.format(
                                    parsed.scheme, parsed.netloc, urllib.quote(parsed.path))
                            else:
                                job_post['inputs'][key] = urllib.quote(parsed.path)
                     
                    #if job_post['archiveSystem']:
                        #logger.debug("%s", job_post['archiveSystem'])
                    try:
                        #data = submit_job(request, request.user.username, job_post)
                        job_post['archiveSystem'] = 'utsa.tseri.corral'
                        logger.debug("%s", job_post['archivePath'])
                        #job_post['archive'] = False # I added this, allows me to download testWrite.txt through cli
                        logger.debug("%s", job_post)
                        user = request.user
                        agave = user.agave_oauth.client 
                        data = agave.jobs.submit(body=job_post)
                        logger.debug("%s", data)
                    except HTTPError as exc:
                        #data = e.json()
                        logger.error('Failed to submit job %s', exc.response.text, exc_info=True)
                        return HttpResponse(json.dumps({'error':'There was an error'}),
                                            content_type='application/json',
                                            status=500)

                # list jobs (via POST?)
                else:
                    limit = request.GET.get('limit', 10)
                    offset = request.GET.get('offset', 0)
                    

            elif request.method == 'GET':
                job_id = request.GET.get('job_id')

                # get specific job info
                if job_id:
                    data = agave.jobs.get(jobId=job_id)
                    q = {"associationIds": job_id}
                    job_meta = agave.meta.listMetadata(q=json.dumps(q))
                    data['_embedded'] = {"metadata": job_meta}

                    archive_system_path = '{}/{}'.format(data['archiveSystem'],
                                                         data['archivePath'])
                    data['archiveUrl'] = reverse(
                        'designsafe_data:data_depot')
                    data['archiveUrl'] += 'agave/{}/'.format(archive_system_path)

                # list jobs
                else:
                    limit = request.GET.get('limit', 10)
                    offset = request.GET.get('offset', 0)
                    data = agave.jobs.list(limit=limit, offset=offset)
            else:
                return HttpResponse('Unexpected service: %s' % service, status=400)

        else:
            return HttpResponse('Unexpected service: %s' % service, status=400)
    except HTTPError as e:
        logger.error('Failed to execute {0} API call due to HTTPError={1}'.format(
            service, e.message))
        return HttpResponse(json.dumps(e.message),
                            content_type='application/json',
                            status=400)
    except AgaveException as e:
        logger.error('Failed to execute {0} API call due to AgaveException={1}'.format(
            service, e.message))
        return HttpResponse(json.dumps(e.message), content_type='application/json',
                            status=400)
    except Exception as e:
        logger.error('Failed to execute {0} API call due to Exception={1}'.format(
            service, e))
        return HttpResponse(
            json.dumps({'status': 'error', 'message': '{}'.format(e.message)}),
            content_type='application/json', status=400)

    return HttpResponse(json.dumps(data, cls=DjangoJSONEncoder),
                        content_type='application/json')

(function(window, angular, $) {
  "use strict";
   angular.module('designsafe').controller('ApplicationFormCtrl',
    ['$scope', '$rootScope', '$localStorage', '$location', '$anchorScroll', '$translate', 'toastr', 'Apps', 'Jobs', 'SystemsService', 'Systems',
    function($scope, $rootScope, $localStorage, $location, $anchorScroll, $translate, toastr, Apps, Jobs, SystemsService, Systems) {

      $localStorage.systemChecks = {};

      $scope.data = {
        messages: [],
        submitting: false,
        needsLicense: false,
        app: null,
        form: {}
      };

      $scope.$on('launch-app', function(e, app) {
        $scope.error = '';

        if ($scope.data.app) {
          $rootScope.$broadcast('close-app', $scope.data.app.id);
        }

        if (app.value.type === 'agave'){
          $scope.data.type = app.value.type;
          Apps.get(app.value.definition.id,app.value.definition.version).then(
            function(resp) {
            // check app execution system
            Systems.getMonitor(resp.data.executionSystem)
              .then(
                function(response){
                  if (response.data.length > 0){
                      // perform check only when monitor is active
                      if (response.data[0].active){
                        if (response.data[0].lastSuccess !== null){
                          var currentDate = new Date();
                          var monitorLastSuccessDate = Date.parse(response.data[0].lastSuccess);
                          var diff = Math.abs((currentDate - monitorLastSuccessDate) / 60000);

                          if (diff > response.data[0].frequency){
                            toastr.warning($translate.instant('error_system_monitor'));
                          }
                        } else {
                          toastr.warning($translate.instant('error_system_monitor'));
                        }
                    }
                  }
                });

            $scope.data.app = resp.data;
            $scope.resetForm();
          });
        } else if (app.value.type === 'html'){
          Apps.getMeta(app.value.definition.id).then(function(resp) {
            if (resp.data.length > 0){
              $scope.data.type = app.value.type;
              $scope.data.app = resp.data[0].value.definition.html;
            }
          });

        }
      });

      $scope.resetForm = function() {
        $scope.data.needsLicense = $scope.data.app.license.type && !$scope.data.app.license.enabled;
        $scope.form = {model: {}, readonly: $scope.data.needsLicense};
        $scope.form.schema = Apps.formSchema($scope.data.app); // data.app is json file for app
        console.log('scope.form.schema')
        console.log($scope.form.schema)
        $scope.form.form = [];
        //$scope.form.schema.properties.parameters.type = "array";
        
        /* Problem with this is that the multiselect is not being added to the model ($scope.form.model) (is this in html, like ng-model?)
           It doesn't seem to be the multiple form.push actions or the dynamic schema (when it's using the settings from the regular schema
           such as the fieldset type). It seems to be an issue with the multiselect type. Now partially solved. Json form needs a key parameter.
           The key corresponds to where it goes in the model. */
        
        console.log("$scope.form.schema.properties.parameters.items.properties");
        var counter = 1;
         if ($scope.form.schema.properties.inputs) {
        _.each(Object.keys($scope.form.schema.properties.inputs.items.properties), function(xkey) {
          var itemString = 'inputs[].'+xkey;
          var stringKey = 'inputs.'+xkey;
          var jsonForm = {
          type: 'fieldset',
          readonly: $scope.data.needsLicense,
          items: [itemString],
          //key: stringKey
          };
          if(counter == 1) {
             console.log("counter = 1");
             jsonForm.title = "Inputs";
          }
          $scope.form.form.push(jsonForm);
          counter = counter + 1;
       });
       }
        
        if ($scope.form.schema.properties.parameters) {
	_.each(Object.keys($scope.form.schema.properties.parameters.items.properties), function(xkey) {  // I added this, need for inputs too but need to add items to inputs
          console.log(xkey);
          var itemString = 'parameters[].'+xkey;
          var stringKey = 'parameters.'+xkey;
          //console.log(itemString);
          console.log("$scope.form.schema.properties.parameters.items.properties[xkey]");
          console.log($scope.form.schema.properties.parameters.items.properties[xkey]);
          var jsonForm = {
          type: 'fieldset',
          readonly: $scope.data.needsLicense,
          items: [itemString],
          //key: stringKey
        };
          if(counter == 1) {
             console.log("counter = 1");
             jsonForm.title = "Inputs";
          }
          jsonForm.placeholder = "Please select one option";
          if($scope.form.schema.properties.parameters.items.properties[xkey].enum) {
             jsonForm.type = "strapselect";
             jsonForm.options = { "multiple": "false"};
             jsonForm.placeholder = "Please select one option";
             jsonForm.titleMap = $scope.form.schema.properties.parameters.items.properties[xkey]['x-schema-form'].titleMap;
             jsonForm.title = $scope.form.schema.properties.parameters.items.properties[xkey].title;
             jsonForm.key = stringKey;
             jsonForm.required = $scope.form.schema.properties.parameters.items.properties[xkey].required;
             if($scope.form.schema.properties.parameters.items.properties[xkey].multiselect == "true") {
              //jsonForm.type = "strapselect";
              jsonForm.options = { "multiple": "true"};
              //jsonForm.titleMap = $scope.form.schema.properties.parameters.items.properties[xkey]['x-schema-form'].titleMap;
              //console.log("titleMap");
              //console.log(jsonForm.titleMap);
              //jsonForm.title = $scope.form.schema.properties.parameters.items.properties[xkey].title;
              //jsonForm.key = stringKey;
              jsonForm.placeholder = "Please select one or more options";
              }
          }
          $scope.form.form.push(jsonForm);
          counter = counter + 1;
        });
        }

        /* inputs */
        var items = [];
        if ($scope.form.schema.properties.inputs) {
          items.push('inputs[]'); // changed (was 'inputs')
        }
        if ($scope.form.schema.properties.parameters) {
          items.push('parameters[]'); // changed (was 'parameters')
        }
        console.log('items') 
        console.log(items) // items[0]= inputs, items[1]= params

        /*
         $scope.form.form.push(
         {
           //key: 'parameters',
          type: 'fieldset',         // 'select' for dropbox - https://github.com/json-schema-form/angular-schema-form/blob/master/docs/index.md#specific-options-and-types
          //key: 'multiselect',
          //options: { "multiple": "true"},
          //type: "strapselect",
          //titleMap: [
        //{"value": 'value1', "name": 'text1'},
        //{"value": 'value2', "name": 'text2'},
        //{"value": 'value3', "name": 'long very very long label3'}
         //],
          readonly: $scope.data.needsLicense,
          title: 'Inputs',
          items: items
          //items: ['parameters[].cropType']
        });*/
         //$scope.form.form.push(jsonForm);
         //$scope.form.form.push(jsonForm2);

        console.log('scope.form.form')
        console.log($scope.form.form)
        console.log('scope.form.form[0].items[0]')
        console.log($scope.form.form[0].items[0])
        console.log('scope.form.model')
        console.log($scope.form.model)

        /* job details */
        $scope.form.form.push({
          type: 'fieldset',
          readonly: $scope.data.needsLicense,
          title: 'Job details',
          items: ['requestedTime','name', 'archivePath']
        });

        /* buttons */
        items = [];
        if (! $scope.data.needsLicense) {
          items.push({type: 'submit', title: 'Run', style: 'btn-primary'});
        }
        items.push({type: 'button', title: 'Close', style: 'btn-link', onClick: 'closeApp()'});
        $scope.form.form.push({
          type: 'actions',
          items: items
        });
      };

      $scope.onSubmit = function(form) {                 // Here is where to concat
        $scope.data.messages = [];
        $scope.$broadcast('schemaFormValidate');
        if (form.$valid) {
          var jobData = {
              appId: $scope.data.app.id+"-"+$scope.data.app.version, // I changed this, was originally-> appId: $scope.data.app.id
              archive: true,
              inputs: {},
              parameters: {}
          };
          var rawJobData = {};
          /* copy form model to disconnect from $scope */
          _.extend(rawJobData, angular.copy($scope.form.model));
          console.log("raw Model");
          console.log(rawJobData);
          var newModel = {};
          //newModel.name = $scope.form.model.name;
          //newModel.parameters = $scope.form.model.parameters[""];
          //newModel.requestedTime = $scope.form.model.requestedTime;
          _.each(Object.keys(rawJobData), function(key) {
            if(key == 'parameters' || key == 'inputs') {
               newModel[key] = {};
               if(key == 'parameters') {
                  if(rawJobData.parameters.hasOwnProperty('')) {
                     console.log("parameters if statement executed");
               	     newModel[key] = rawJobData[key][""];
                  }
               }
               if(key == 'inputs' && rawJobData.inputs.hasOwnProperty('')) {
                  if(rawJobData.inputs.hasOwnProperty('')) {
                     console.log("inputs if statement executed");
               	      newModel[key] = rawJobData[key][""];
                  }
               }
               //console.log("are there quotes");
               //console.log(rawJobData["parameters"][""]);
               //console.log(_.has(rawJobData,'parameters.\"\"'));
               //console.log(rawJobData.parameters.hasOwnProperty(''));
               //console.log(rawJobData[key][""]);
               _.each(Object.keys(rawJobData[key]), function(key2) {
                        if(key2 != "") {
                           var str = "";
                           console.log(rawJobData[key][key2][0]);
                           console.log(typeof $scope.form.model[key][key2]);
                           if(typeof $scope.form.model[key][key2] == 'object') {
		                   for (var i = 0; i < $scope.form.model[key][key2].length; i++) {
		                        if($scope.form.model[key][key2].length == 1 || i == ($scope.form.model[key][key2].length-1)) {
		                   	   str = str + rawJobData[key][key2][i];
		                        } else {
		                           str = str + rawJobData[key][key2][i] + "~";
		                        }
		                   }
                            } else {
                                   str = rawJobData[key][key2];
                           }
               		   //newModel[key][key2] =  $scope.form.model[key][key2];
                           //newModel[key][key2] =  rawJobData[key][key2].toString();
                           newModel[key][key2] = str;
                        }
               }); 
            } else {
               newModel[key] = $scope.form.model[key];
            }
          });
          console.log("$scope.form.model");
          console.log($scope.form.model);
          console.log("New Model");
          console.log(newModel);
          //_.extend(jobData, angular.copy($scope.form.model));
          _.extend(jobData, angular.copy(newModel));

          /* remove falsy input/parameter */
          _.each(jobData.inputs, function(v,k) {   
            if (_.isArray(v)) {
              v = _.compact(v);
              if (v.length === 0) {
                delete jobData.inputs[k];
              }
            }
          });
          _.each(jobData.parameters, function(v,k) { 
            if (_.isArray(v)) {
              v = _.compact(v);
              if (v.length === 0) {
                delete jobData.parameters[k];
              }
            }
          });

          $scope.data.submitting = true;
          Jobs.submit(jobData).then(
            function(resp) {
              $scope.data.submitting = false;
              $rootScope.$broadcast('job-submitted', resp.data);
              $scope.data.messages.push({
                type: 'success',
                header: 'Job Submitted Successfully',
                body: 'Your job <em>' + resp.data.name + '</em> has been submitted. Monitor its status on the right.'
              });
              $scope.resetForm();
              refocus();
            }, function(err) {
              $scope.data.submitting = false;
              $scope.data.messages.push({
                type: 'danger',
                header: 'Job Submit Failed',
                body: 'Your job submission failed with the following message:<br>' +
                      '<em>' + (err.data.message || 'Unexpected error') + '</em><br>' +
                      'Please try again. If this problem persists, please ' +
                      '<a href="/help" target="_blank">submit a support ticket</a>.'
              });
              refocus();
            });
        }
      };

      function refocus() {
        $location.hash('workspace');
        $anchorScroll();
      }

      function closeApp() {
        $scope.data.app = null;
        $scope.data.appLicenseEnabled = false;
      }

      $scope.$on('close-app', closeApp);

      $scope.closeApp = function() {
        $rootScope.$broadcast('close-app', $scope.data.app.id);
        closeApp();
      };
    }]);
})(window, angular, jQuery);

(function(window, angular, $, _) {
  "use strict";
  angular.module('designsafe').factory('Jobs', ['$http', function($http) {
    var service = {};

    service.list = function(options) {
      var params = {
        limit: options.limit || 10,
        offset: options.offset || 0
      };
      return $http.get('/api/workspace/jobs/', {      // these did not originally have the / at the end. Added the trailing slash to fix an error
        params: params});
    };

    service.get = function(uuid) {
      return $http.get('/api/workspace/jobs/', {
        params: {'job_id': uuid}
      });
    };

    service.submit = function(data) {
      return $http.post('/api/workspace/jobs/', data);
    };

    return service;
  }]);
})(window, angular, jQuery, _);

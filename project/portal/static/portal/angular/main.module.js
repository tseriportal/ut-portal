(function(window, angular, $, _){
  "use strict";
  angular.module('designsafe', ['ng.modernizr']).config(['$httpProvider', function($httpProvider) {
        $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
          $httpProvider.defaults.xsrfCookieName = 'csrftoken';
            $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
  }]);
})(window, angular, jQuery, _);

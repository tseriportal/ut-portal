# BASE STATIC FILES

This directory is meant to hold all custom static files necessary.
Meaning, any front end scripting file or image needed for the project which is meant to be used across the project or does not falls into a specific django app.

# README #

## UT Portal ##

* UT Portal django project
* v0.1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

## SETUP ##

* Requirements
  * Vagrant installation:
    * ansible http://docs.ansible.com/ansible/intro_installation.html
    * vagrant https://www.vagrantup.com/docs/installation/
  * Docker Installation:
    * docker https://docs.docker.com/engine/installation/
    * docker-compose https://docs.docker.com/compose/install/

### Vagrant Setup ###

1. Create working directory.
    - `mkdir ut-portal`
2. Create provision directory and clone provision scripts.
    - `mkdir -p ut-portal/provision && git clone https://bitbucket.org/taccaci/ut-portal-provision.git ut-portal/provision`
3. Create empty project directory.
    - `mkdir -p ut-portal/project`
3. Create ansible vars file with necessary values.
    - `cp ut-portal/provision/conf/provisioning/ansible/playbooks/group_vars/local.example.yml ut-portal/provision/conf/provisioning/ansible/playbooks/group_vars/local.yml`
4. Run vagrant.
    - `cd ut-portal/provision && vagrant up`
5. Create django project directory and virtualenv (local and optional).
    - `mkdir -p ut-portal/project/.env && virtualenv -p python2 ut-portal/project/.env/project && source ut-portal/project/.env/project/bin/activate`

### Docker Setup ####

1. Create working directory
    - `mkdir ut-portal`
2. Clone repo
    - `git clone https://bitbucket.org/taccaci/ut-portal-portal.git ut-portal/project`
3. Create env file with necessary avlues.
    - `cp ut-portal/project/conf/env_files/portal.sample.env ut-portal/project/conf/env_files/portal.env`
4. Build docker image
    1. With docker-compose
        - `docker-compose -f ut-portal/project/conf/docker/docker-compose.yml build`
    2. With docker build
        - `cd ut-portal/project && docker build -f conf/docker/Dockerfile -t ut-portal/portal:local`
5. Create django project directory and virtualenv (local and optional).
    - `mkdir -p ut-portal/project/.env && virtualenv -p python2 ut-portal/project/.env/project && source ut-portal/project/.env/project/bin/activate`

### Project Setup ###

After setting up your environment either with Vagrant or Docker, the project settings have to be configured

1. Copy all \*.example.\* files removing the word `example` from the name.
    -  `find * -name *.example.* | while read line; do cp $line ${line/example\./}; done`
2. Update local settings files accordingly
    - To see which files need to be locally configured run this: `find * -name *.example.* | while read line; do echo ${line/example\./}; done`
3. Docker:
    1. Setup database
        -  `docker-compose -f conf/docker/docker-compose-dev.all.debug.yml up mysql`
    2. Run the entire project 
        -  `docker-compose -f conf/docker/docker-compose-dev.all.debug.yml up`
    3. Run migrations
        - `docker exec -it ut_django ./manage migrate`
    4. Run npm install
        - `docker exec -it ut_django npm install`
    5. Run collectstatic
        - `docker exec -it ut_django collectstatic`
4. Vagrant:
    1. ssh into the VM
        - `vagrant ssh`
    2. Run migrations
        - `./manage.py migrate`
    3. Run npm install
        - `npm install`
    4. Run collectstatic
        - `./manage.py collectstatic`

## Accessing the portal (locally) ##

### Docker ###

1. Add a record in your local `hosts` file: `localhost portal.dev`
2. Access `https://portal.dev`

### Vagrant ###

The VM created by Vagrant will run `nginx` and `uwsgi` services which will serve a django project.

You can setup your local dns to point to that VM.

1. Get ip from box.
2. Add a record in your local `hosts` file: `<ip> portal.dev`.
3. Access `https://portal.dev`.

You can also use django's `runserver`

1. Get ip from box.
2. Add a record in your local `hosts` file: `<ip> portal.dev`.
3. Ssh into the VM and run django's runserver.
  - `vagrant ssh ut-portal`
  - `source /srv/www/.envs/project/bin/activate`
  - `python /srv/www/project/manage.py runserver 0.0.0.0:8000`
4. Access `https://project.dev`.
